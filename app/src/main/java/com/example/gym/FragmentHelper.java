package com.example.gym;

import android.app.Fragment;

import com.kartiks.utility.LoggerGeneral;

public class FragmentHelper {
	
	
	public static Fragment getFragment(@SuppressWarnings("rawtypes") Class currentClass){

		LoggerGeneral.info("returning class " + currentClass.getName());

        if(currentClass.getSimpleName().equals(FragmentSearch.class.getSimpleName())){
            return FragmentSearch.getInstance();
        }

        return null;
	}

}
