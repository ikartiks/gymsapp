package com.example.gym.utility;

import android.os.AsyncTask;

import com.example.gym.ActivityBase;
import com.example.gym.FragmentBase;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kartiks.utility.LoggerGeneral;

import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

/**
 * Created by kartik on 22/6/15.
 * Sample examples
     new RestAsyncTask(activity).execute(Constants.getBaseUrl()+"test/greeting", HttpMethod.DELETE,greeting,null);
     new RestAsyncTask(activity).execute(Constants.getBaseUrl()+"test/greeting", HttpMethod.PUT,greeting,Greeting.class);
     new RestAsyncTask(activity).execute(Constants.getBaseUrl()+"test/greeting", HttpMethod.GET,null,Greeting.class);

     FileSystemResource fileSystemResource= new FileSystemResource("/storage/sdcard/Download/ic_launcher.png");
     MultiValueMap<String, Object> p = new LinkedMultiValueMap<String, Object>();
     p.add("file", fileSystemResource);
     p.add("name", "it works");
 *
 */
public class RestTemplateAsyncTask extends AsyncTask<Object,Integer,Object>{

    ActivityBase activityBase;
    FragmentBase fragmentBase;

    String url;
    Constants constants;
    public enum Type{
        Activity,
        Fragment
    }
    Type type;

    public RestTemplateAsyncTask(ActivityBase activity, String url) {

        this.activityBase=activity;
        this.type=Type.Activity;
        this.url=url;
        constants=new Constants(activity);
        LoggerGeneral.info("making request " + url);
    }

    public RestTemplateAsyncTask(FragmentBase fragment, String url) {

        this.fragmentBase=fragment;
        this.type=Type.Fragment;
        this.url=url;
        constants=new Constants(fragment.getActivity());
        LoggerGeneral.info("making request " + url);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(this.type==Type.Activity){

            activityBase.showLoader();
        }else if (this.type==Type.Fragment){

            fragmentBase.showLoader();
        }

    }


    /**
    *
    * @params 0 HttpMethod
    * @params 1 Object to pass to server, NULLABLE
    * @params 2 Class of object to receive from server, NULLABLE
    *
    * */
    @Override
    protected Object doInBackground(Object[] params) {

        HttpMethod method= (HttpMethod) params[0];
        Object objectToPass=params[1];
        Class returnObjectClass= (Class) params[2];

        RestTemplate restTemplate = new RestTemplate();


        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        if(objectToPass instanceof MultiValueMap)
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        //rest template adds content length to 2 bytes by default, so it gives below error
        // content-length promised 2 bytes, but received 0
        //to remove that when delete request is made ,we do following
        if(returnObjectClass==null)
            headers.add("Content-Length", "0");


        //this is to convert timestamp to date object
        GsonBuilder builder = new GsonBuilder();
        //builder.excludeFieldsWithoutExposeAnnotation();

        GsonHttpMessageConverter jsonConverter = new GsonHttpMessageConverter();
        builder.setDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
        //builder.setDateFormat(DateFormat.FULL,DateFormat.FULL);

        Gson gson =builder.create();
        jsonConverter.setGson(gson);


        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());//kuch toh hoga iska bhi
        restTemplate.getMessageConverters().add(jsonConverter);// for normal java objects passed in entity
        restTemplate.getMessageConverters().add(new FormHttpMessageConverter());//for multipart/form data

        restTemplate.setErrorHandler(new MyErrorResponseHandler());

        HttpEntity httpEntity =new HttpEntity(objectToPass,headers);

        ResponseEntity<?> response=null;
        response = restTemplate.exchange(url, method, httpEntity,returnObjectClass);

        if(response.hasBody()){

            Object responseBody = response.getBody();
            LoggerGeneral.info(" ---------------------- " );
            LoggerGeneral.info(url +" " + responseBody);
            return  responseBody;

        }else{

            LoggerGeneral.info(" ---------------------- " );
            LoggerGeneral.info(url +" null"+response);
            return null;
        }




    }

    public class MyErrorResponseHandler implements ResponseErrorHandler {
        @Override
        public void handleError(ClientHttpResponse response) throws IOException {

            LoggerGeneral.e("response code is " + response.getStatusCode() + " " + response.getStatusText());
            final String responseString= IOUtils.toString(response.getBody());
            LoggerGeneral.e("error resposne message is "+responseString);

            //since data is still executing in background thread, so call this
            //and we cant even call runOnUIThread ,or run showToast everytime on UI Thread
            if(type==Type.Activity){

                activityBase.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        activityBase.hideLoader();
                        activityBase.showCustomMessage(responseString);
                    }
                });

            }else if (type==Type.Fragment){

                fragmentBase.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        fragmentBase.hideLoader();
                        fragmentBase.showCustomMessage(responseString);
                    }
                });

            }
            cancel(false);//this is important, otherwise thread pool executor
            // wont allow you to execute another
            // request as this one somehow never finishes

            return;//PS this wont throw IO Exception,never ever remove this
        }

        @Override
        public boolean hasError(ClientHttpResponse response) throws IOException {

            if ((response.getStatusCode() == HttpStatus.OK) || (response.getStatusCode() == HttpStatus.NO_CONTENT)) {
                return false;
            }
            return true;
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();

    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);

        if(this.type==Type.Activity){

            activityBase.hideLoader();


        }else if (this.type==Type.Fragment){

            fragmentBase.hideLoader();


        }

    }
}