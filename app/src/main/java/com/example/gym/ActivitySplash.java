package com.example.gym;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class ActivitySplash extends ActivityBase {

    Context context=ActivitySplash.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    Intent intent=new Intent(context,ActivityLogin.class);
                    startActivity(intent);
                    finish();
                }
            }
        }).start();

    }

}
