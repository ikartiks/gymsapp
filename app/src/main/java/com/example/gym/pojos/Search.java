package com.example.gym.pojos;

/**
 * Created by kartikshah on 11/10/15.
 */
public class Search {

    String name;
    String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
