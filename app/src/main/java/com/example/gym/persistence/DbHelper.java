package com.example.gym.persistence;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.Stack;

public class DbHelper extends SQLiteOpenHelper {

	public static final int DATABASE_VERSION = 1;
	public static final String DATABASE_NAME = "gre.db";

	static DbHelper mInstance = null;
	public SQLiteDatabase greDb = null;
	public Context context;


	/**
	 * constructor should be public to prevent direct instantiation. make call
	 * to static factory method "getInstance()" instead.
	 */
	private DbHelper(Context ctx) {
		super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
		this.context = ctx;
	}

	public static DbHelper getInstance(Context context) {
		/**
		 * use the application context as suggested by CommonsWare. this will
		 * ensure that you dont accidentally leak an Activitys context (see this
		 * article for more information:
		 * http://developer.android.com/resources/articles
		 * /avoiding-memory-leaks.html)
		 */
		if (mInstance == null) {
			mInstance = new DbHelper(context.getApplicationContext());
		}
		return mInstance;
	}

	Stack<Long> openCloseStack = new Stack<Long>();

	private synchronized SQLiteDatabase openDatabse() {

		openCloseStack.push(System.currentTimeMillis());
		if (greDb == null || !greDb.isOpen()) {
			// LoggerGeneral.e("opening new db");
			greDb = getWritableDatabase();
		}
		return greDb;
	}

	private synchronized void closeDatabse() {

		openCloseStack.pop();
		if (greDb != null && openCloseStack.empty()) {
			// LoggerGeneral.e("closing the db");
			greDb.close();
		}
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

}