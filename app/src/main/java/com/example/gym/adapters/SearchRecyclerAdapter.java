package com.example.gym.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.gym.FragmentSearch;
import com.example.gym.R;
import com.example.gym.pojos.Search;

import java.util.ArrayList;

public class SearchRecyclerAdapter extends RecyclerView.Adapter<SearchRecyclerAdapter.ViewHolder>{

	ArrayList<Search> searchArrayList;

    FragmentSearch fragmentSearch;

//  OnItemClickListener clickListener;
//	public interface OnItemClickListener {
//		   public void onItemClick(View view, int position);
//	}

//	public void setOnItemClickLickListener(OnItemClickListener listener){
//
//		clickListener=listener;
//	}
	
	
	public SearchRecyclerAdapter(FragmentSearch fragmentSearch,ArrayList<Search> searchArrayList) {
		this.searchArrayList = searchArrayList;
		this.fragmentSearch=fragmentSearch;
	}

	@Override
	public int getItemCount() {
		
		return searchArrayList.size();
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		
		Search t = searchArrayList.get(position);
		holder.name.setText(t.getName());

        holder.getItemView().setTag(t);
		//holder.description.setText(t.getPtDescription());
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent,int viewType) {
		
		View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflate_search, parent, false);
		ViewHolder vh = new ViewHolder(v);
		return vh;
	}
	
	


	
	public class ViewHolder extends RecyclerView.ViewHolder implements OnClickListener{

		TextView name;
        ImageView image;
        View itemView;
		
		public ViewHolder(View itemView) {
			super(itemView);
            this.itemView=itemView;
			name = (TextView) itemView.findViewById(R.id.Name);
			image = (ImageView) itemView.findViewById(R.id.Image);
			itemView.setOnClickListener(this);
		}

        public View getItemView() {
            return itemView;
        }

        @SuppressWarnings("deprecation")
		@Override
		public void onClick(View v) {
			
//			if(clickListener!=null){
//				clickListener.onItemClick(v, getPosition()); //OnItemClickListener mItemClickListener;
//			}
            fragmentSearch.searchSelection((Search)v.getTag());
		}

	}

}
