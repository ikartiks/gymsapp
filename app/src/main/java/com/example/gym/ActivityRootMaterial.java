package com.example.gym;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.SearchManager;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.example.gym.persistence.AppPreferences;
import com.example.gym.utility.TypefaceSpan;
import com.kartiks.utility.LoggerGeneral;

/**
 * @author kartikshah 
 * 
 * only reason we are using action bar activity is that it has 
 * support method for matrial designs setSupportActionBar(Support toolbar)
 * 
 * and hence the changes in menu xml
 * 
 *  Ps we are not and will not use support fragment due to reduced animations on that part
 *  hence we shall always have min api level as 14
 *
 */
public class ActivityRootMaterial extends ActivityBase {

	Context context = ActivityRootMaterial.this;
	Activity activity = ActivityRootMaterial.this;

	AppPreferences appPreferences;
	Resources resources;
    Handler handler;

	DrawerLayout mDrawerLayout;
	LinearLayout drawer;// ,contentFrame;

	Toolbar toolbar;

    LinearLayout rootContainer;


	public enum MenuType {
		// we may use this later
	};

	// for fragments
	// PS always content fragment should be frame layout
	// drawer should be anything other than that ,other wise fragments go in
	// wrong container--no idea y
	// if step 2 causes drawer fragment to wrap content
	// ,setLayoutParams(newLayoutParams(LayoutParams.MATCHPARENT,LayoutParams.MATCHPARENT))
	// and then return the view. like in case of new FragmentNavigation()
	// or use View v=inflater.inflate(R.layout.fragment_navigation, container,
	// false); insted of
	// View v=inflater.inflate(R.layout.fragment_navigation, null);

	// second method is better
	FrameLayout contentFrame;

	FragmentManager fm;
    SearchView searchView;

	// String currentFragmentTag = "";



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);

		// Associate searchable configuration with the SearchView
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		final MenuItem searchMenuItem = menu.findItem(R.id.SearchMenu);

        searchView = (SearchView) searchMenuItem.getActionView();
		searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        //searchView.setQueryRefinementEnabled(true);
		//search view on close listener doesnt work so...use menu item collapsed
		searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String arg0) {


                LoggerGeneral.e("submit");
                //fm.popBackStackImmediate();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String arg0) {

                FragmentSearch fragmentSearch = (FragmentSearch) fm.findFragmentByTag(FragmentSearch.class.getName());
                if (fragmentSearch != null)
                    fragmentSearch.addDataToAdapter();
                return false;
            }
        });

        MenuItemCompat.setOnActionExpandListener(searchMenuItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                fm.popBackStackImmediate();
                return true;
            }
        });
        MenuItemCompat.setActionView(searchMenuItem, searchView);


		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
        switch (id){
            case R.id.SearchMenu:

                LoggerGeneral.e("add fragment");
                addContentFragment(FragmentSearch.class,null);
                break;

        }
		return super.onOptionsItemSelected(item);
	}


    @Override
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_root_material);

        resources = getResources();
        appPreferences = AppPreferences.getAppPreferences(context);
        handler=new Handler();

        rootContainer=(LinearLayout)findViewById(R.id.RootContainer);
        //note to be called before hiding them.

        toolbar=(Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		//toolbar.setDisplayShowHomeEnabled(true);
		//toolbar.setDisplayShowTitleEnabled(true);
		//toolbar.setDisplayUseLogoEnabled(true);
		toolbar.setNavigationIcon(R.drawable.ic_drawer_light);
		
		toolbar.setNavigationOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				openOrCloseDrawer();
			}
		});
		//toolbar.setLogo(R.drawable.ic_launcher);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

		contentFrame = (FrameLayout) findViewById(R.id.content_frame);
		drawer = (LinearLayout) findViewById(R.id.left_drawer);

		fm = getFragmentManager();
		// fm.beginTransaction().setCustomAnimations(enter, exit, popEnter,
		// popExit);

		//fm.beginTransaction().replace(drawer.getId(), new FragmentNavigation()).commit();

		//addContentFragment(FragmentWordListRecyle.class, null);

		if (isTablet()) {
			// device is a tablet disable navigation drawer;
			mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
		}

	}

	@SuppressLint("RtlHardcoded")
	public void openOrCloseDrawer() {

        if(!isTablet()){
            if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                mDrawerLayout.closeDrawer(Gravity.LEFT);
            } else {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        }

	}


	public void initializeBodyHeader(String bodyTitlex) {

		SpannableString s = new SpannableString(bodyTitlex);
		s.setSpan(new TypefaceSpan(this, "os.ttf"), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		// Update the action bar title with the TypefaceSpan instance
		toolbar.setTitle(s);

		// invalidateOptionsMenu();
	}
	



    @SuppressLint("RtlHardcoded")
    public void onBackPressed() {

        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawer(Gravity.LEFT);
            return;
        }

        if (fm.getBackStackEntryCount() > 1) {

            // ideally should be 0,but in this case its 1 as we want the first
            // fragment to remain

            //getBackStackEntryAt function takes index where 0 represents bottom
            //and top represents the current fragment ,fm.getBackStackEntryCount()-1
            //so if you want to avoid a particular fragment which is about to come use fm.getBackStackEntryCount()-2
            FragmentManager.BackStackEntry bse=fm.getBackStackEntryAt(fm.getBackStackEntryCount()-2);

            LoggerGeneral.e("bse is " + bse.getName());

            //since we want to display testlist and not fragment tests.
            //if(bse.getName().equals(FragmentTests.class.getName()))
                //fm.popBackStackImmediate();

            fm.popBackStackImmediate();


        }else{

            //LoggerGeneral.e("this was the last");
            //super.onBackPressed();
            showEndPop();
            //finish();
        }
        //causes fragment to be removed
        //super.onBackPressed();
    };

	@SuppressWarnings("rawtypes")
	public void addContentFragment(Class currentClass, Bundle b) {

        //android actually adds all fragments to back stack, addToBackStack is method only made for us
        // to help remember which fragments were added to back stack ,
        // if you dont want any fragment to be added to the actual back stack ,
        // you will have to remove it manually by calling ft.remove(fragment)


        String name=currentClass.getName();
		if (!isTablet()){
			
			//LoggerGeneral.info("device not tablet");
			if (mDrawerLayout.isDrawerOpen(drawer)) {
				mDrawerLayout.closeDrawer(drawer);
				//LoggerGeneral.info("closing");
			}
		}


		// check if fragment is in back stack
		Fragment f = fm.findFragmentByTag(currentClass.getName());
        FragmentTransaction ft = fm.beginTransaction();

		if (f == null) {

            // no such fragment added
			f = FragmentHelper.getFragment(currentClass);

			f.setArguments(b);

		} else {


//            if(currentClass.getName().equals(FragmentTests.class.getName())){
//
//                LoggerGeneral.e("removeing test fragment");
//                ft.remove(f);
//                ft.commit();//imp here, need to commit first
//
//
//                ft = fm.beginTransaction();
//                f = FragmentHelper.getFragment(currentClass);
//                f.setArguments(b);
//            }

            LoggerGeneral.e("adding from back stack "+currentClass.getName());

			// pop it from back stack
            if (f.isVisible()) {
				return;
			}
            // below line caues an empty fragment in this case,when we go  from 1 to 2 to 3
            //and from navigation fragment back to 1
			//fm.popBackStack(currentClass.getName(), FragmentManager.POP_BACK_STACK_INCLUSIVE);

            //dont remove also ,everytime you add a fragment it creates a clone somehow, hence all history is stored
            //ft.remove(f);
		}

        ft.setCustomAnimations(R.anim.slide_in_from_right,R.anim.slide_out_to_right,
                R.anim.slide_in_from_left,R.anim.slide_out_to_left);
        ft.replace(contentFrame.getId(), f, currentClass.getName());

        // adds fragment that was previously in the container to back
        // stack,with following tag since there was none,shows bg
		ft.addToBackStack(currentClass.getName());
        ft.commit();
	}


}
