package com.kartiks.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.kartiks.sample.R;

/**
 * Created by kartik on 6/7/15.
 */
public class BaseDialog extends Dialog{

    Context context;
    int bgResourceId;

    private BaseDialog(Context context) {
        super(context);
        this.context=context;
    }

    private BaseDialog(Context context, int theme) {
        super(context, theme);
        this.context=context;
    }

    protected BaseDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        this.context=context;
    }

    /**
     *
     * Our custom constructor
     *
     * @param context
     * @param style aka the theme for dialog constructor eg. R.style.OurLightStyleBackword
     * @param fullScreen if pop should be full screen
     * @param bgResourceId the background overlay   eg R.drawable.overlay
     *
     */
    public BaseDialog(Context context, int style,boolean fullScreen,int bgResourceId) {
        super(context,style);
        //style is nothing but the theme
        this.context=context;

        this.bgResourceId=bgResourceId;
        Activity activity= (Activity) context;
        DisplayMetrics displaymetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        if(fullScreen)
            this.getWindow().setLayout(width, height);

        this.getWindow().setBackgroundDrawableResource(bgResourceId);
        this.setCancelable(true);
        this.setCanceledOnTouchOutside(true);
        this.getWindow().setGravity(Gravity.CENTER);
        this.setContentView(R.layout.base_dialog);

    }

    public void setContent(View v){
        RelativeLayout mainContainer= (RelativeLayout) findViewById(R.id.MainContainer);
        mainContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        LinearLayout container=(LinearLayout)findViewById(R.id.Container);
        container.setBackgroundResource(bgResourceId);
        container.addView(v,new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT));
    }

}